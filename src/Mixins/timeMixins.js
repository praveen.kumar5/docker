import moment from 'moment'

export default {
    computed: {
        getCurrentTime: function (){
            return moment().format('MMMM Do YYYY, h:mm:ss a');            
        }
    },

    methods: {
        
    }
}
import TranslatorApp from "./Components/TranslatorApp";
import ArticleApp from "./Components/ArticleApp";

export default [
    {
        name: 'defaultRoute',
        path: '/',
        component: TranslatorApp
    },
    {
        name: 'translatorRoute',
        path: '/translator',
        component: TranslatorApp,
        props: true
    },
    {
        name: 'articleRoute',
        path: '/article/:articleTitle',
        component: ArticleApp,
        props: true
    },
]